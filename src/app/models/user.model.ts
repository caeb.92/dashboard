export interface User {
  avatar_url: string;
  name: string;
  location: string;
}