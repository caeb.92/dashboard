import { Component, OnInit } from '@angular/core';
// Angular router
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  jsonObject = {
    "status": {
      "code": 200,
      "message": "servicio ejecutado con éxito."
    },
    "resumen": [
      {
        "flujo_id": 42,
        "flujo": "check-eliminar-productos",
        "errores": "4",
        "correctas": "25",
        "total": "29"
      },
      {
        "flujo_id": 51,
        "flujo": "check-eliminar-productos2",
        "errores": "3",
        "correctas": "3",
        "total": "6"
      },
      {
        "flujo_id": 21,
        "flujo": "check-compra-retiro-tienda",
        "errores": "2",
        "correctas": "0",
        "total": "2"
      },
      {
        "flujo_id": 27,
        "flujo": "check-multifecha-despacho",
        "errores": "21",
        "correctas": "2",
        "total": "23"
      },
      {
        "flujo_id": 47,
        "flujo": "check-garantia-extendida",
        "errores": "2",
        "correctas": "0",
        "total": "2"
      },
      {
        "flujo_id": 53,
        "flujo": "check-carro-compra-incompatible",
        "errores": "0",
        "correctas": "3",
        "total": "3"
      }
    ]
  }
  hide: boolean;
  constructor(private route: ActivatedRoute) {
    if (this.route.children.length > 0) {
      this.hide = true;
    }
  }

  ngOnInit() {
  }

}
