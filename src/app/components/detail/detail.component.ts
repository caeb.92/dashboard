import { Component, OnInit, Input } from '@angular/core';
// Services
import { ApiService } from 'src/app/services/api.service';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  item$;
  id: string;
  constructor(private api: ApiService, private route: ActivatedRoute) { }
  getData() {
    this.item$ = this.route.paramMap.pipe(
      switchMap(params => {
        this.id = params.get('id');
        return null;
      })
    );
  }

  getUserById(id: string) {
    this.api.getUserById(id);
    console.log(id)
  }

  ngOnInit() {
    this.getData();
  }

}
