import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-rest',
  templateUrl: './rest.component.html',
  styleUrls: ['./rest.component.scss']
})
export class RestComponent implements OnInit {
  userData: User;
  constructor(private api: ApiService) {
    // Al renderizar el componente ejecuta el metodo
    this.getUserData();
  }

  getUserData() {
    this.api.getUser().subscribe(response => {
      this.userData = response;
    }), error => {
      console.log(error)
    }
  }
  ngOnInit() {
  }

}
