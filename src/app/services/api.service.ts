import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// Rxjs
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user.model';
// Models

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  readonly endpoint = 'https://api.github.com/users/';
  constructor(private httpClient: HttpClient) { }

  getUser(): Observable<User> {
    return this.httpClient.get<User>(this.endpoint + 'caeb92').pipe(
      map(response => response)
    );
  }

  getUserById(userName: string): Observable<User> {
    return this.httpClient.get<User>(this.endpoint + userName).pipe(
      map(response => response)
    );
  }
}
