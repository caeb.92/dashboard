import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Components
import { DashboardComponent } from '../app/components/dashboard/dashboard.component'
import { ConfigurationComponent } from '../app/components/configuration/configuration.component'
import { DetailComponent } from '../app/components/detail/detail.component'
import { RestComponent } from '../app/components/rest/rest.component'

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'configuration', component: ConfigurationComponent },
  { path: 'detail', component: DetailComponent },
  { path: 'rest', component: RestComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
